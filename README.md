> Stata连享会 &ensp;   [计量专题](https://www.lianxh.cn/news/46917f1076104.html)  || [直播间](http://lianxh.duanshu.com) || [知乎推文](https://www.zhihu.com/people/arlionn/) 

![](https://images.gitee.com/uploads/images/2020/0229/091709_2cef60f0_1522177.png)


## 简介 - Causal Inference Book

> **Book：** Hernán MA, Robins JM (2020).  **Causal Inference: What If**. Boca Raton: Chapman & Hall/CRC. 

![](https://images.gitee.com/uploads/images/2020/0228/175928_beb2892e_1522177.png)

> 「**因果推断**」最近很火，让我们很是 **恼火**!

&emsp; 

今天要介绍的这本书叫 **《Causal Inference: What If》**，是统计学大牛 J. Robins 和 Hernán 共同完成的一本颇具影响力的书。这本书已经面世好几年的时间了，目前放在他们主页上的是最新修订版。两位作者非常慷慨，直接在自己的主页上放了书稿的 PDF 原本。

来自不同领域的学者们也对这本书给予了很大的支持。提供了这本书部分章节的实现代码，包括 Stata, R, Python, SAS。

其中，Stata 实现过程由 Eleanor Murray 提供。 

第 1-10 章重点讲述一些基本概念，通过大量的简单实例和图形的方式，深入浅出地介绍了因果推断中的核心概念和方法。第 11-18 章讲述了各种各样用于进行因果推断的模型，包括工具变量法 (IV)、倾向得分匹配分析 (PSM)、调节效应、结构方程等。第 19-25 章介绍了较为复杂的情形，如面板数据、
动态处理效应、反馈效应等。 

为了便于大家学习这本书里面的内容，我们从作者的主页以及 Github 仓库上，把所有相关的资料都下载下来并整理成了目前的码云仓库，以便于国内用户访问。 

## 书稿、数据和程序

- Hernán MA, Robins JM (2020). Causal Inference: What If. Boca Raton: Chapman & Hall/CRC. [[PDF在线浏览]](https://quqi.gblhgk.com/s/880197/eQHgb0bnGgYeafcp)，[[作者书稿主页]](https://www.hsph.harvard.edu/miguel-hernan/causal-inference-book/)
- **Stata 实现过程：** Chp 11-17 各章的 Stata 实现代码来自 [Eleanor Murray - Github](https://github.com/eleanormurray/causalinferencebook_stata)。
- **R, Python, SAS 实现过程 ：** 请访问 [[作者书稿主页]](https://www.hsph.harvard.edu/miguel-hernan/causal-inference-book/)，包括书中第 11-17 章中所用的数据和实现过程。 

## 使用方法

进入码云仓库后，点击橘红色【克隆/下载】按钮，即可将仓库中所有文件下载到本地电脑中。

在 Stata 中打开【_Main.do】文档后，按提示执行命令即可打开各章对应的 dofiles。



 
&emsp; 

> **连享会-直播间** 开通了！      
> &emsp;         
> <http://lianxh.duanshu.com>     
> &emsp; 
> ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-直播间海报400.png)    
> &emsp; 
> 长按二维码观看最新直播课


&emsp; 

---
## 最新课程 
- **F.** [直播-R语言初识](https://lianxh.duanshu.com/#/brief/course/a719037536de4812a630a599f8cd7b43)，嘉宾：游万海, 2020年3月2日，19:00-21:00。
- **E.**  [直播-经济学中的大数据应用](https://lianxh.duanshu.com/#/brief/course/da1a75bc3acc4e238f489af3367efa26)，嘉宾：李兵 (中山大学). [「课程详情」](https://www.lianxh.cn/news/761e6bbfe07a8.html)
- **D.** [直播-空间计量全局模型及Matlab实现](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), 嘉宾：范巧。[「课程详情」](https://www.lianxh.cn/news/6fdb88905419e.html)
- **C.** [直播-动态面板数据模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e), 嘉宾：连玉君。[「课程详情」](https://www.lianxh.cn/news/594aa12c096ca.html)
- **B.** [直播-实证研究设计](https://mp.weixin.qq.com/s/NGwsr92_Vr1DGRbVqDVQIA)，嘉宾：连玉君。[「课程详情」](https://www.lianxh.cn/news/2f31aa3347e83.html)    
- **A.** [直播-文本分析与爬虫专题](https://gitee.com/arlionn/Course/blob/master/Done/2020Text.md)，**2020.3.28-29， 4.4-5**，嘉宾：司继春, 游万海。[「课程详情」](https://www.lianxh.cn/news/88426b2faeea8.html)      
- **O.** [公开课-直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38)，嘉宾：连玉君，**免费**. [「课程详情」](https://gitee.com/arlionn/PanelData)     


&emsp; 
---

## 附：仓库、书中目录节选

![](https://images.gitee.com/uploads/images/2020/0228/175928_c715e450_1522177.png)

![](https://images.gitee.com/uploads/images/2020/0228/175928_dd118ae6_1522177.png)

![](https://images.gitee.com/uploads/images/2020/0228/175928_231b53dc_1522177.png)

&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- &#x1F33A; **你的颈椎还好吗？** 为了保护颈椎，您可以前往 [::连享会-主页::](https://www.lianxh.cn) (在浏览器地址栏中输入 lianxh.cn 即可) 或 [::连享会-知乎专栏::](https://www.zhihu.com/people/arlionn/) (在知乎中搜索「连享会」即可) 查看往期推文，并将上述网址添加到浏览器收藏夹中或把您喜欢的推文【收藏】起来。
- &#x1F4C1; **往期推文分类查看：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。
- &#x1F4A5; **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：`DID，RDD，PSM，面板，IV，合成控制法，内生性, Probit, plus，Profile, Bootstrap, MC, 交乘项, 平方项, 工具, 软件, Sai2, gInk, Annotator, 手写批注, 直击面板数据, 连老师, 直播, 空间计量, 爬虫, 文本分析, 正则, Markdown幻灯片, marp, 盈余管理, ` ……。


---

![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2020/0228/175928_38d04783_1522177.png)



